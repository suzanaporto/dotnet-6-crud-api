﻿namespace WebApi.Services;

public class EmailService
{
    public IStorageService StorageInterface { get; set; }
    public EmailService(IStorageService StorageInterface)
    {
        this.StorageInterface = StorageInterface;
    }

    public void SendEmail(string email)
    {
        Console.WriteLine($"Sending email to {email}");
        StorageInterface.Add(email);
    }
}
