public interface IStorageService {
    public void Add(string value);
    public List<string> GetAll();
}